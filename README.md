# Views Minimum Condition

Views Minimum is a condition plugin that will evaluate a selected view and
its selected display for results and return true or false if the View is empty
or not (has at least 1 result). This will make it possible to not only hide the
area where the View would be, but also be capable of hiding (or showing) other
areas of a page based on this condition.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_minimum).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_minimum).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires Views.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module.
2. Navigate to elements in admin that use conditions (Blocks, Contexts, etc).
3. Add the `"View is not empty"` condition and select a View.
4. Save and continue.

When saved, this element will no longer appear if the condition is not met.


## Maintainers

- Kevin Quillen - [kevinquillen](https://www.drupal.org/u/kevinquillen)

**Supporting organizations:**
- [Velir](https://www.drupal.org/velir)
