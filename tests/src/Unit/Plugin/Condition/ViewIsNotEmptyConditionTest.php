<?php

namespace Drupal\Tests\views_minimum\Unit\Plugin\Condition;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\views\ViewEntityInterface;
use Drupal\views\ViewExecutable;
use Drupal\views_minimum\Plugin\Condition\ViewIsNotEmpty;

/**
 * Contains tests for the "view_is_not_empty" condition plugin.
 *
 * @group views_minimum
 */
class ViewIsNotEmptyConditionTest extends UnitTestCase {

  /**
   * Generate an entityTypeManagerMock that handles Views mocks.
   */
  public function entityTypeManagerMock($view_mock) : EntityTypeManagerInterface {
    $entityStorage = $this->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $entityStorage->expects($this->any())
      ->method('load')
      ->willReturn($view_mock);

    $entityTypeManagerMock = $this->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $entityTypeManagerMock->expects($this->any())
      ->method('getStorage')
      ->willReturn($entityStorage);

    return $entityTypeManagerMock;
  }

  /**
   * Generate a Views mock with results.
   */
  public function getViewMock(int $number_results = 1) {
    $viewExecutableMock = $this->getMockBuilder(ViewExecutable::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['setDisplay', 'execute'])
      ->getMock();

    $viewExecutableMock->expects($this->any())
      ->method('setDisplay')
      ->willReturn(TRUE);

    $viewExecutableMock->expects($this->any())
      ->method('execute')
      ->willReturn(TRUE);

    $viewExecutableMock->result = array_fill(0, $number_results, 'Test');

    $viewMock = $this->getMockBuilder(ViewEntityInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $viewMock->expects($this->any())
      ->method('getExecutable')
      ->willReturn($viewExecutableMock);

    return $viewMock;
  }

  /**
   * Generate a logger service mock.
   */
  public function loggerMock() {
    $loggerMock = $this->getMockBuilder(LoggerChannelFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $loggerMock->expects($this->any())
      ->method('get')
      ->willReturn(LoggerChannelInterface::class);

    return $loggerMock;
  }

  /**
   * Generate a messenger service mock.
   */
  public function messengerMock() {
    return $this->getMockBuilder(MessengerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Plugin data provider.
   *
   * @return array
   *   An array containing plugin configuration and test values.
   */
  public function configurationProvider() {
    return [
      [
        [
          'view_id' => '',
          'has_minimum' => FALSE,
          'minimum_count' => 1,
        ],
        0,
        TRUE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => FALSE,
          'minimum_count' => 1,
        ],
        1,
        TRUE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => TRUE,
          'minimum_count' => 3,
        ],
        1,
        FALSE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => TRUE,
          'minimum_count' => 3,
        ],
        4,
        TRUE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => TRUE,
          'minimum_count' => 3,
        ],
        3,
        TRUE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => TRUE,
          'minimum_count' => "3",
        ],
        3,
        TRUE,
      ],
      [
        [
          'view_id' => 'foo:bar',
          'has_minimum' => FALSE,
          'minimum_count' => 3,
        ],
        1,
        TRUE,
      ],
    ];
  }

  /**
   * Test various states of the ViewIsNotEmpty condition plugin.
   *
   * @param array $configuration
   *   The plugin configuration to test.
   * @param int $results
   *   The number of results to return from Views.
   * @param bool $expected
   *   The expected result from the plugins evaluate method.
   *
   * @dataProvider configurationProvider
   */
  public function testViewIsNotEmptyConditions(array $configuration, int $results, bool $expected) {
    $view_mock = $this->getViewMock($results);

    $plugin = new ViewIsNotEmpty(
      $configuration,
      'view_is_not_empty',
      [],
      $this->entityTypeManagerMock($view_mock),
      $this->loggerMock(),
      $this->messengerMock(),
    );

    $this->assertSame($expected, $plugin->evaluate());
  }

}
