<?php

namespace Drupal\views_minimum\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\area\View;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'View is not empty' condition.
 *
 * @Condition(
 *   id = "view_is_not_empty",
 *   label = @Translation("View is not empty"),
 * )
 */
class ViewIsNotEmpty extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get('php');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_id' => '',
      'has_minimum' => FALSE,
      'minimum_count' => 1,
      'check_empty' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $config = $this->getConfiguration();
    $id = $config['view_id'];

    if (!empty($id)) {
      $data = explode(':', $config['view_id']);
      $view_id = $data[0];
      $view_display = $data[1];

      return $this->t('View "@view_id" with the "@view_display" selected.',
        [
          '@view_id' => $view_id,
          '@view_display' => $view_display,
        ]
      );
    }
    else {
      return $this->t('No view selected.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $views = $this->entityTypeManager->getStorage('view')->loadMultiple();
    $config = $this->getConfiguration();
    $displays = ['' => $this->t('- Please select -')];

    foreach ($views as $view) {
      foreach ($view->get('display') as $display) {
        $displays[$view->label()][$view->id() . ':' . $display['id']] = $display['display_title'];
      }
    }

    $form['view_id'] = [
      '#type' => 'select',
      '#title' => $this->t('View & View Display'),
      '#description' => $this->t('Select the View and appropriate display to check for this condition. Leave empty to bypass this condition.'),
      '#options' => $displays,
      '#default_value' => $config['view_id'] ?? array_key_first($displays),
      '#attributes' => ['data-views-minimum' => 'view_id'],
    ];

    $form['has_minimum'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enforce a minimum result threshold'),
      '#description' => $this->t('Enable this to gain the option to set a minimum result value. This will also check that the view has results greater than or equal to this amount.'),
      '#default_value' => $config['has_minimum'] ?? FALSE,
      '#attributes' => ['data-views-minimum' => 'has_minimum'],
      '#states' => [
        'visible' => [
          ':input[data-views-minimum="view_id"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['check_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check for Views embedded in the Empty area'),
      '#description' => $this->t('Enable this to additionally check for Views added as in the "No Results" area of a parent View to count toward the total results.'),
      '#default_value' => $config['check_empty'] ?? FALSE,
    ];

    $form['minimum_count'] = [
      '#type' => 'number',
      '#min' => 1,
      '#title' => $this->t('Minimum result count'),
      '#description' => $this->t('Set the total of results a View must have for this condition. For example, if you only want to show a View if it has 3 or more results, set this to 3.'),
      '#default_value' => $config['minimum_count'] ?? 1,
      '#states' => [
        'visible' => [
          ':input[data-views-minimum="view_id"]' => ['!value' => ''],
          ':input[data-views-minimum="has_minimum"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('negate', (bool) $form_state->getValue('negate'));
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['view_id'] = $form_state->getValue('view_id');
    $this->configuration['has_minimum'] = $form_state->getValue('has_minimum');
    $this->configuration['minimum_count'] = $form_state->getValue('minimum_count');
    $this->configuration['check_empty'] = $form_state->getValue('check_empty');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Collects all possible Views from a parent View.
   *
   * This checks for Views embedded in the Empty display.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The View to inspect.
   * @param array $collection
   *   The current array of Views embedded to the parent View.
   *
   * @return array
   *   The array of all Views embedded to the parent View.
   */
  protected function collectViews(ViewExecutable $view, array $collection = []): array {
    if (!empty($view->empty)) {
      foreach ($view->empty as $item) {
        if ($item instanceof View) {
          if (!empty($item->options['view_to_insert'])) {
            // Borrow code from \Drupal\views\Plugin\views\area\View::render.
            [$view_name, $display_id] = explode(':', $item->options['view_to_insert']);

            $view = $this->entityTypeManager->getStorage('view')->load($view_name)->getExecutable();

            if (empty($view) || !$view->access($display_id)) {
              return [];
            }

            $view->setDisplay($display_id);

            // Avoid recursion.
            $view->parent_views += $item->view->parent_views;
            $view->parent_views[] = "$view_name:$display_id";

            // Check if the view is part of the parent views of this view.
            $search = "$view_name:$display_id";
            if (in_array($search, $item->view->parent_views)) {
              $this->messenger
                ->addError($this->t("Recursion detected in view @view display @display.", [
                  '@view' => $view_name,
                  '@display' => $display_id,
                ]));
            }
            else {
              $collection[] = $view;
              $view->execute();
              return $this->collectViews($view, $collection);
            }
          }
        }
      }
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $config = $this->getConfiguration();

    if (empty($config['view_id'])) {
      return TRUE;
    }

    $id = $config['view_id'];
    $enforce_minimum = (bool) $config['has_minimum'];
    $check_empty = (bool) $config['check_empty'];
    $minimum = (int) $config['minimum_count'];
    $total_results = 0;

    $data = explode(':', $id);
    $view_id = $data[0];
    $view_display = $data[1];

    try {
      $view = $this->entityTypeManager->getStorage('view')->load($view_id)->getExecutable();

      if (isset($view)) {
        $view->setDisplay($view_display);
        $view->execute();
        $total_results = (int) count($view->result);

        if ($check_empty) {
          $views = $this->collectViews($view);

          foreach ($views as $view) {
            $total_results += (int) count($view->result);
          }
        }
      }
      else {
        $this->logger->error('ViewIsNotEmpty condition failed to execute - the View "@view" or its display "@display" could not be loaded. Check that they still exist and or the display id did not change. If the display id changed, you need to reconfigure the conditions that were using them.',
          [
            '@view' => $view_id,
            '@display' => $view_display,
          ]
        );
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Error encountered in the @plugin condition: @error', [
        '@plugin' => $this->getPluginId(),
        '@error' => $e->getMessage(),
      ]);
    }

    return ($enforce_minimum) ? ($total_results >= $minimum) : ($total_results > 0);
  }

}
